/*
 * kafka-streams-triple-analyzer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.Klaxon
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import java.util.*

class KafkaTopology(private val properties: Properties, log: Logger) {

    private val analyzer = TripleAnalyzer(log)

    fun build(): Topology {
        val builder = StreamsBuilder()

        builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .flatMapValues { value -> analyzer.process(value.data) }
            .map { _, value -> KeyValue(value["subject"] as String, SbMetadataModel()
                .setData(Klaxon().toJsonString(value))
                .setEsBulkAction(EsBulkActions.INDEX)
                .setEsIndexName(properties.getProperty("elastic.index"))) }
            .to(properties.getProperty("kafka.topic.sink"))

        return builder.build()
    }
}
