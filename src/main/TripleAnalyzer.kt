/*
 * triple analyzer
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.logging.log4j.Logger
import java.nio.charset.Charset
import java.util.regex.Pattern

class TripleAnalyzer(private val log: Logger) {

    private val triplePattern = Pattern.compile("^<(.*?)> <(.*?)> (<(.*?)>|\"(.*?)\"(@([a-z]{2,3})(-([A-Za-z]+)?)?)?|\"(.*?)\"\\^\\^<(.*?)>) .$")

    private val events = createSet("events")
    private val organisations = createSet("organisations")

    private fun createSet(filename: String): Set<String> {
        val set = mutableSetOf<String>()
        ClassLoader.getSystemResourceAsStream("$filename.csv")?.bufferedReader(Charset.defaultCharset())?.lines()?.forEach {
            set.add(it.substringAfterLast('/'))
        }
        if (set.isEmpty()) {
            log.error("Could not load instance classes from $filename. The set is empty.")
        }
        return set
    }

    fun process(value: String): List<Map<String, Any>> {
        val triples = mutableListOf<Triple>()
        for (line in value.split("\n"))
        {
            try {
                triples.add(parseTriple(line))
            }   catch (ex: TripleParseError) {
                log.error(ex.message)
            }
        }

        val map = mutableMapOf<String, Any>()

        map["subject"] = triples[0].s
        map["types"] = mutableListOf<String>()
        map["instanceOf"] = mutableListOf<String>()
        map["predicates"] = mutableListOf<String>()
        map["objects"] = mutableListOf<String>()
        map["languageTags"] = mutableListOf<String>()
        map["languages"] = mutableListOf<String>()
        map["countries"] = mutableListOf<String>()
        map["dataTypes"] = mutableListOf<String>()
        map["triplesCount"] = triples.size
        triples.forEach {
            if (it.p == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type") {
                val typeList = map["types"]
                typeList as MutableList<String>
                typeList.add(it.o)
            }
            if (it.p == "http://www.wikidata.org/prop/direct/P31") {
                val typeList = map["instanceOf"]
                typeList as MutableList<String>
                typeList.add(it.o)
                if (it.o.substringAfterLast("/") in events) {
                    map["isWikidataEvent"] = true
                }
                if (it.o.substringAfterLast("/") in organisations) {
                    map["isWikidataOrganisation"] = true
                }

            }

            val predicateList = map["predicates"]
            predicateList as MutableList<String>
            predicateList.add(it.p)

            val objectList = map["objects"]
            objectList as MutableList<String>
            objectList.add(it.o)

            if (it.languageTag != null) {
                val languageList = map["languageTags"]
                languageList as MutableList<String>
                languageList.add(it.languageTag)
            }

            if (it.language != null) {
                val languageList = map["languages"]
                languageList as MutableList<String>
                languageList.add(it.language)
            }

            if (it.countryTag != null) {
                val countryList = map["countries"]
                countryList as MutableList<String>
                countryList.add(it.countryTag)
            }

            if (it.type != null) {
                val typeList = map["dataTypes"]
                typeList as MutableList<String>
                typeList.add(it.type)
            }
        }

        return if (map.isEmpty()) {
            listOf<Map<String, Any>>()
        } else {
            listOf(map)
        }
    }


    private fun parseTriple(line: String): Triple {
        val matcher = triplePattern.matcher(line)
        if (matcher.matches()) {
            val subject = matcher.group(1)
            val predicate = matcher.group(2)
            val objectValue = matcher.group(4)
            val literal = matcher.group(5)
            val languageTag = matcher.group(6)
            val language = matcher.group(7)
            val countryCode = matcher.group(9)
            val typeLiteral = matcher.group(10)
            val type = matcher.group(11)
            return when {
                objectValue != null -> Triple(subject, predicate, objectValue, null, null, null, null)
                literal != null -> Triple(subject, predicate, literal, languageTag, language, countryCode, null)
                typeLiteral != null -> Triple(subject, predicate, typeLiteral, null, null, null, type)
                else -> throw TripleParseError("No object found for $line.")
            }
        } else {
            throw TripleParseError("Could not match $line")
        }
    }
}