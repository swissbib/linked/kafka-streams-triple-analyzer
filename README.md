## Triple Analyzer

A small application which takes resources per message in n-triple format. The 
application then collects objects, predicates, type definitions, language tags
and counts the number triples. 

This then is packed into a suitable json structure for indexing in elasticsearch.

The purpose is to be able to see what kind structure the data has in a dataset.

For example to make a list of all present predicates.

It is not really usefull to look at objects / literals, as they are indexed 
in a single field.